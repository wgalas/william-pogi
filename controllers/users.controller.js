const model = require("../models");
const UserModel = model.User;

exports.getUsers = async (req, res, next) => {
  const users = await UserModel.findAll();
  res.send(users);
};

exports.createUser = async (req, res, next) => {
  const user = await UserModel.create(req.body);
  res.send(user);
};

exports.showUser = async (req, res, next) => {
  const user = await UserModel.findByPk(req.params.id);
  res.send(user);
};
