const models = require("../models");
const PostModel = models.Post;
exports.getPosts = async (req, res, next) => {
  const posts = await PostModel.findAll();
  return res.send(posts);
};

exports.createPost = async (req, res, next) => {
  const post = await PostModel.create(req.body);
  return res.send(post);
};

exports.showPost = async (req, res, next) => {
  try {
    const post = await PostModel.findByPk(req.params.id);

    if (!post) {
      throw new Error("Not found");
    }
    res.send(post);
  } catch (error) {
    res.status(404);
    res.send({ message: error.message });
  }
};

exports.updatePost = async (req, res, next) => {
  try {
    const post = await PostModel.update(req.body, {
      where: { id: req.params.id },
    });

    if (post != 1) {
      throw new Error("Not found");
    }
    res.send({ message: "ok" });
  } catch (error) {
    res.status(404);
    res.send({ message: error.message });
  }
};

exports.deletePost = async (req, res, next) => {
  try {
    const post = await PostModel.destroy({
      where: { id: req.params.id },
    });

    if (post != 1) {
      throw new Error("Not found");
    }
    res.send({ message: "ok" });
  } catch (error) {
    res.status(404);
    res.send({ message: error.message });
  }
};
