var express = require("express");
var postsController = require("../controllers/posts.controller");
var router = express.Router();

router.get("/", postsController.getPosts);
router.get("/:id", postsController.showPost);
router.post("/", postsController.createPost);
router.put("/:id", postsController.updatePost);
router.delete("/:id", postsController.deletePost);

module.exports = router;
