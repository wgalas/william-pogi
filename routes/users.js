var express = require("express");
var usersController = require("../controllers/users.controller");
var router = express.Router();

router.get("/", usersController.getUsers);
router.post("/", usersController.createUser);
router.get("/:id", usersController.showUser);

module.exports = router;
